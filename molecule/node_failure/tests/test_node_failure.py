import os
import pytest

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture
def stop_keepalived():
    host = testinfra.utils.ansible_runner.AnsibleRunner(
       os.environ['MOLECULE_INVENTORY_FILE']).get_host(testinfra_hosts[0])

    host.run('systemctl stop keepalived')


def test_keepalived_vip_switches(ansible_vars, stop_keepalived):
    vip = ansible_vars['keepalived_vip_address']
    vrrp_interface = ansible_vars['keepalived_vrrp_interface']

    host = testinfra.utils.ansible_runner.AnsibleRunner(
        os.environ['MOLECULE_INVENTORY_FILE']).get_host(testinfra_hosts[1])

    assert vip in host.interface(vrrp_interface).addresses
