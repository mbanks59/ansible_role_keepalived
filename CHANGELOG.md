# 1.0.0 (2021-01-27)


### Bug Fixes

* adding repoman ([541a8b0](https://gitlab.com/dreamer-labs/maniac/ansible_role_keepalived/commit/541a8b0))


### Features

* Modify vrrp_script syntax ([d902192](https://gitlab.com/dreamer-labs/maniac/ansible_role_keepalived/commit/d902192))


### BREAKING CHANGES

* Modifies vrrp_script param syntax

This commit changes the syntax for specifying a vrrp_script
and properly namespaces some keepalived variables. Additionally
we add molecule testing scenarios for a node failure, normal
two node, and placing vrrp scripts.
